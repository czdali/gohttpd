//
// gohttpd - simple static http server
// Author: Dalibor Sramek, dali@insula.cz
// Version: 0.1
// Date: 2017/11/09
// Copyright: MIT License
//

package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
)

func main() {
	fmt.Println("===> gohttpd v0.1")
	logf := flag.String("l", "", "write log to a file")
	port := flag.String("p", "8100", "port to serve on")
	root := flag.String("d", ".", "server root directory")
	flag.Parse()
	// Open a log file if we should use one
	if *logf != "" {
		file, err := os.OpenFile(
			*logf, os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0666,
		)
		if err == nil {
			defer file.Close()
			log.SetOutput(file)
			fmt.Printf("logging to %s \n", *logf)
		} else {
			// Just print an error mesage, the log output remains unchanged
			log.Printf("error opening log file: %v\n", err.Error())
		}
	}
	// Create a file-serving handler
	fs := http.FileServer(http.Dir(*root))
	// Add a logging handler to the line and attach it to the root URL
	http.Handle("/", logger(fs))
	// Run the server
	log.Printf("serving %s on HTTP port: %s\n", *root, *port)
	log.Fatal(http.ListenAndServe(":"+*port, nil))
}

// Logging middleware
func logger(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Printf("%s from %q %s %s", r.Proto, r.RemoteAddr, r.Method, r.URL)
		h.ServeHTTP(w, r) // call next handler in line
	})
}
